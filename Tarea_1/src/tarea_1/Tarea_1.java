package tarea_1;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
/**
 *
 * @author Jair
 */
public class Tarea_1 {
    
    
Map<String, Integer> MAP = new HashMap<String, Integer>();

//Separa las palabras de la cadena string
  public void buscar_palabra(String texto){
  StringTokenizer st = new StringTokenizer (texto);
    String s="";
     while (st.hasMoreTokens())
        {
            s = st.nextToken();
          comparar_palabra(s);
        }
  }
  
  
  public void comparar_palabra(String palabra){
     
  if(MAP.containsKey(palabra)){
          //Si existe guarda el valor
         int aux= MAP.get(palabra);
         MAP.put(palabra, aux+1);
  }else{
          //Si no, inserta una nueva palabra
          MAP.put(palabra, 1);
        }
      
  }
 
  public void numero_palabras(){
      System.out.println("Palabras distintas : "+ MAP.size());
      System.out.println("Palbras");
      System.out.println("mapa: "+MAP.entrySet());
  }
    
    public static void main(String[] args) 
    {
        
       PdfReader obj = new PdfReader("C:\\Users\\Jair\\Desktop\\tarea2.pdf");
       int num_pag= obj.getNumberOfPages();
        System.out.println("Numero de paginas :"+num_pag);
        for(int i=1; i<num_pag; i++){
            pag += PdfTextExtractor.getTextFromPage(obj, i);
        }
        Tarea_1 m =new Tarea_1 ();
        // Envia cadena completa para separar palabras
      m.buscar_palabra(pag);
      
      m.numero_palabras();
        
       
    }
}
